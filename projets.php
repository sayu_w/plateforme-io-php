<?php include "header.php"; ?>
<?php include "navbar.php"; ?>

<header></header>
  <div class="page-header header-filter clear-filter black-filter" data-parallax="true">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
           <div class="header_mobile"></div>		    
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main main-raised ">
    <div class="container background_body">
      <div class="section text-center">
        <div class="row">
          <div class="col-md-10 ml-auto mr-auto">
            <div class="space-30"></div>

            <div class="space-70"></div>
          </div>
        </div>
      </div>
    </div>
  </div>   
</div>

<?php include "footer.php"; ?>