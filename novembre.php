<?php include "header.php"; ?>
<?php include "navbar.php"; ?>

<header></header>
  <div class="page-header header-filter clear-filter black-filter" data-parallax="true">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
           <div class="header_mobile"></div>		       		    
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main main-raised ">
    <div class="container background_body">
      <div class="section text-center">
        <div class="row">
          <div class="col-md-10 ml-auto mr-auto">
            <div class="space-30"></div>               
              <div class="card">
                <p class="codepen" data-height="500" data-theme-id="light" data-default-tab="result" data-user="MiisterK" data-slug-hash="RwWYraV" style="height: 1000px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="RwWYraV">
                    <span>See the Pen <a href="https://codepen.io/MiisterK/pen/RwWYraV">
                    RwWYraV</a> by Kevin Tourat (<a href="https://codepen.io/MiisterK">@MiisterK</a>)
                    on <a href="https://codepen.io">CodePen</a>.</span>
                  </p>
                  <script async src="https://static.codepen.io/assets/embed/ei.js"></script>
              </div>
            <div class="space-70"></div>
          </div>
        </div>
      </div>
    </div>
  </div> 
</div>

<?php include "footer.php"; ?>