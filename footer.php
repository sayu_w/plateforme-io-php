<footer class="footer1 footer-default">
    <div class="container">
        <p class="bottom-footer footer">
            <a class="nav-link" href="index.html">
                <img class="svg-two" src="image/smart-home.svg" alt="home">
            </a>                            
            <a target="_blank" rel="noopener" href="https://maxlefort35.gitlab.io/portfolio-ml/"> &copy; Maxime Lefort</a>
            <a target="_blank" rel="noopener" class="space-footer" href="https://tourat_kevin.gitlab.io/portfolio-io/" >&copy; Kévin Tourat</a>           
        </p>
    </div>
</footer>
    <!--   Core JS Files   -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script> -->
    <!-- <script src="./assets/js/core/popper.min.js" type="text/javascript"></script> -->
    <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>   
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
</body>
 
</html>