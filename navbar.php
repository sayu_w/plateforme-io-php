<nav class="navbar navbar-img navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">         
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="acceuil.php">Plateforme - io</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
       </div>
       <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="dropdown nav-item ">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" >
                        <i class="material-icons">folder</i>Mes cours
                    </a>
                    <div class="dropdown-menu dropdown-with-icons"  style="width: 700px;">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <p>Periode de cours</p>
                                    <!-- 1 -->
                                    <a href="septembre.php" class="dropdown-item">
                                        <i class="material-icons">filter_1</i>Septembre
                                    </a>            
                                    <a href="octobre.php" class="dropdown-item">
                                        <i class="material-icons">filter_2</i>Octobre
                                    </a>
                                    <a href="novembre.php" class="dropdown-item">
                                        <i class="material-icons">filter_3</i>Novembre
                                    </a>
                                    <a href="decembre.php" class="dropdown-item">
                                        <i class="material-icons">filter_4</i>Décembre
                                    </a>
                                    <a href="janvier.php" class="dropdown-item">
                                        <i class="material-icons">filter_5</i>Janvier
                                    </a>
                                </div>
                                <div class="col-sm">
                                    <!-- 2 -->
                                    <p>Stage / Examen</p>
                                    <a href="fevrier.php" class="dropdown-item">
                                        <i class="material-icons">filter_6</i>Février
                                    </a>
                                    <a href="mars.php" class="dropdown-item">
                                        <i class="material-icons">filter_7</i>Mars
                                    </a>
                                    <a href="avril.php" class="dropdown-item">
                                        <i class="material-icons">filter_8</i>Avril
                                    </a>
                                    <a href="mai.php" class="dropdown-item">
                                        <i class="material-icons">filter_9</i>Mai
                                    </a>           
                                    <a href="examen.php" class="dropdown-item">
                                        <i class="material-icons">school</i>Examen
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                    <!-- tuto -->
                <li class="dropdown nav-item ">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" >
                        <i class="material-icons">emoji_objects</i>Tuto
                    </a>
                    <div class="dropdown-menu dropdown-with-icons"  style="width: 400px;">
                        <div class="container">
                            <div class="row">        
                                <div class="col-sm">                             
                                    <!-- liste tuto -->
                                    <a href="tuto_css.php" class="dropdown-item">
                                        <i class="material-icons">filter_1</i>Tuto en cours..
                                    </a>
                                    <a href="tuto_css.php" class="dropdown-item">
                                        <i class="material-icons">filter_2</i>Tuto en cours..
                                    </a>               
                                    <a href="tuto_css.php" class="dropdown-item">
                                        <i class="material-icons">filter_3</i>Tuto en cours..
                                    </a>
                                    <a href="tuto_css.php" class="dropdown-item">
                                        <i class="material-icons">filter_4</i>Tuto en cours..
                                    </a>               
                                    <a href="tuto_css.php" class="dropdown-item">
                                        <i class="material-icons">filter_5</i>Tuto en cours..
                                    </a>                               
                                    <a href="tuto_css.php" class="dropdown-item">
                                        <i class="material-icons">filter_6</i>Tuto en cours..
                                    </a>
                                </div>                                                  
                            </div>
                        </div>
                    </div>
                </li> 
                <li class="nav-item dropdown">
                    <li class="nav-item">
                        <a class="nav-link" href="videos.php">
                            <i class="material-icons">video_call</i>Vidéos
                        </a>
                    </li>            
                    <li class="nav-item">
                        <a class="nav-link" href="projets.php">
                            <i class="material-icons">gavel</i>Projets
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.php">
                            <i class="material-icons">chat</i>Contact
                        </a>
                    </li>
                </li>         
                <li class="nav-item">
                    <a target="_blank" rel="noopener" class="nav-link" href="https://twitter.com/">
                        <i class="fa fa-twitter"></i>             
                    </a>            
                </li>
                <li class="nav-item">
                    <a target="_blank" rel="noopener" class="nav-link" href="https://gitlab.com/">
                        <i class="fa fa-gitlab"></i>
                    </a>           
                </li>
                <li class="nav-item">
                    <a target="_blank" rel="noopener" class="nav-link" href="https://www.linkedin.com/">
                        <i class="fa fa-linkedin"></i>
                    </a>           
                </li>
            </ul>        
        </div>
    </div>
</nav>