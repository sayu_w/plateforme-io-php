<?php include "header.php"; ?>
<?php include "navbar.php"; ?>

  <header></header>
   <!-- <div class="space-110"></div> -->
  <div class=" page-header header-filter clear-filter black-filter" data-parallax="true">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
           <div class="header_mobile"></div>		      	    
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main main-raised ">
    <div class="container">
      <div class="section text-center">
        <div class="space-110"></div>
          <h2 class="title">Documentation vidéos</h2>              
        <div class="jumbotron">
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe1"></div>        
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe2"></div>
          </div>
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe3"></div>           
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe4"></div>
          </div>
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe5"></div>         
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe6"></div>
          </div>
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe7"></div>          
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe8"></div>
          </div>
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe9"></div>          
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe10"></div>
          </div>
        </div>
        <div class="space-30"></div> 
        <button type="button" class="btn btn-outline-primary btn-lg"><i class="material-icons">skip_previous</i> Pages</button>
        <button type="button" class="btn btn-outline-primary btn-lg">Pages<i class="material-icons">skip_next</i></button>
        <div class="space-30"></div>
      </div>
    </div>
  </div>
</div>

<?php include "footer.php"; ?>