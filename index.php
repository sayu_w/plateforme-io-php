<?php include "header.php"; ?>

<form class="acceuil"> 
  <div class="form">
    <h3>Corsaire 2.0</h3>
    <div class="input">
      <a class="btn btn-outline-primary" href="acceuil.php" role="button">Se connecter</a>           
      <div class="space-30"></div>
      <a class="btn btn-outline-primary" href="https://classroom.google.com" role="button">Google classroom</a> 
      <div class="space-30"></div>         
      <a class="btn btn-outline-primary" href="https://solacroupecole.fr3.quickconnect.to/" role="button">NAS Solacroup</a>
      <div class="space-30"></div>                    
    </div>
  </div>     
</form>  

<?php include "footer.php"; ?>