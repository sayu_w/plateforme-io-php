<?php include "header.php"; ?>
<?php include "navbar.php"; ?>
  
  <header></header>
   <div class="page-header header-filter clear-filter black-filter" data-parallax="true">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
           <div class="header_mobile"></div>
		       
		    
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="main main-raised ">
    <div class="container background_body">
      <div class="section text-center">
        <div class="row">
          <div class="col-md-10 ml-auto mr-auto">

               <!-- cours gratuit en ligne -->

               <div class="space-30"></div>
               <section>
                 <h1>Cours gratuit sur Udemy !</h1>
                <div class="row">                  
                  <div class="col-sm-6">
                    <div class="card">                     
                      <div class="card-body">
                        <div class="space-30"></div>            
                        <img class="img-sm" src="image/keyboard-690066_640.jpg" alt="">
                        <div class="space-50"></div>
                        <a type="button" href="https://www.udemy.com/course/introduction-au-developpement-web/" class="btn btn-outline-primary">Accéder au cours</a>
                        <div class="space-30"></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6 ">
                    <div class="card">
                      <h2>Introduction au Développement Web</h2>
                      <div class="card-body">
                        <h3>Prérequis</h3>
                        <ul>
                          <li>Aucune compétence particulière n'est requise</li>
                          <li>N'importe quel ordinateur</li>
                          <li>Aucun logiciel particulier n'est requis</li>
                        </ul>
                        <h3>Description</h3>
                        <p>Dans ce cours, je vous offre <b>une vue d'ensemble sur le développement Web.</b> On va tout bien expliquer depuis le début.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <!-- cours gratuit en ligne 2 -->

              <div class="space-30"></div>
              <section>
               <div class="row">
                 <div class="col-sm-6">
                   <div class="card">
                    <h2>Découvrir Javascript en 30 minutes</h2>
                     <div class="card-body">            
                      <h3>Prérequis</h3>
                      <ul>
                        <li>Aucune compétence particulière n'est requise</li>
                        <li>N'importe quel ordinateur - Windows, Mac, etc...</li>
                        <li>Aucun logiciel particulier n'est requis</li>
                      </ul>
                      <h3>Pourquoi apprendre Javascript ?</h3>
                      <p>Le but de ce petit cours c’est de vous faire découvrir l’univers Javascript ! Si vous devez choisir d’apprendre qu’un seul et unique langage de programmation, je vous recommande vivement d’apprendre Javascript. Pourquoi? Dans ce cours, je vais vous montrer que c’est le langage le plus populaire.</p>
                     </div>
                   </div>
                 </div>
                                 
                  <div class="col-sm-6">
                    <div class="card">                     
                      <div class="card-body">
                        <div class="space-30"></div>            
                        <img class="img-sm" src="image/javascript-736400_640.png" alt="">
                        <div class="space-50"></div>
                        <a type="button" href="https://www.udemy.com/course/quest-ce-que-javascript/" class="btn btn-outline-primary">Accéder au cours</a>
                        <div class="space-30"></div>
                      </div>
                    </div>
                  </div>
                </section>

             <!-- cours gratuit en ligne 3 -->

             <div class="space-30"></div>
             <section>
             <div class="row">                  
               <div class="col-sm-6">
                 <div class="card">                     
                   <div class="card-body">
                     <div class="space-30"></div>            
                     <img class="img-sm" src="image/css3-1841488_640.jpg" alt="">
                     <div class="space-50"></div>
                     <a type="button" href="https://www.udemy.com/course/apprendre-a-creer-des-sites-professionnels-avec-html5-et-css/" class="btn btn-outline-primary">Accéder au cours</a>
                     <div class="space-30"></div>
                   </div>
                 </div>
               </div>

               <div class="col-sm-6 ">
                 <div class="card">
                   <h2>Apprendre à créer des sites professionnels avec HTML5 et CSS</h2>
                   <div class="card-body">
                     <h3>Prérequis</h3>
                     <ul>
                       <li>Aucune compétence particulière n'est requise</li>
                       <li>N'importe quel ordinateur</li>
                       <li>Aucun logiciel particulier n'est requis</li>
                     </ul>
                     <h3>Description</h3>
                     <p>Vous avez toujours voulu créer le site de vos rêves ? Commencez par les bases grâce à ce cours gratuit ! Ce cours gratuit vous permettra de survoler quelques notions en HTML5 et CSS3, pour vous faire une idée du langage.</p>
                     <p>Vidéo à la demande d'une durée de 3 h 11 min</p>
                    </div>
                 </div>
               </div>
             </div>
           </section>

           <div class="space-30"></div>

           <section>
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                 <h2>Méthode express' : créer un site avec WordPress</h2>
                 
                  <div class="card-body">
                   <p>Découvrez WordPress pour créer des sites internet</p>            
                   <h3>Prérequis</h3>
                   <ul>
                     <li>Seule de la motivation et de la bonne volonté. Une utilisation régulière de l'outil informatique et d'internet est un plus non négligeable.</li>
                   </ul>
                   <h3>Description</h3>
                   <p>A toute personne souhaitant apprendre à créer des sites internet avec WordPress rapidement.
                    WordPress est utilisé par plus d'un quart des sites web au monde, maîtriser son interface d'administration et savoir gérer 
                    un site WordPress sont des compétences de plus en plus appréciées dans le monde professionnel. </p>
                  </div>
                </div>
              </div>
                              
               <div class="col-sm-6">
                 <div class="card">                     
                   <div class="card-body">
                     <div class="space-30"></div>            
                     <img class="img-sm" src="image/wordpress-1810453_640.jpg" alt="">
                     <div class="space-50"></div>
                     <a type="button" href="https://www.udemy.com/course/methode-express-creer-un-site-avec-wordpress/" class="btn btn-outline-primary">Accéder au cours</a>
                     <div class="space-30"></div>
                   </div>
                 </div>
               </div>
             </section>

             <div class="space-30"></div>
             <h3>MOOC</h3>

             <section>
               <div class="row">
                 <div class="col-sm-6">
                    <div class="card">                     
                      <div class="card-body">
                        <div class="space-30"></div>            
                        <img class="img-sm" src="image/ANSSI.jpg" alt="">
                        <div class="space-50"></div>
                        <a type="button" href="https://www.udemy.com/course/methode-express-creer-un-site-avec-wordpress/" class="btn btn-outline-primary">Accéder au cours</a>
                        <div class="space-30"></div>
                      </div>
                    </div>
                  </div>
              
                 <div class="col-sm-6">
                  <div class="card">
                   <h2>Bienvenue sur le MOOC <br> de l'ANSSI.</h2>
                   
                    <div class="card-body">
                     <h3>Description</h3>
                     <p>Vous y trouverez l’ensemble des informations pour vous initier à la cybersécurité, approfondir vos connaissances,
                        et ainsi agir efficacement sur la protection de vos outils numériques. Ce dispositif est accessible gratuitement 
                        jusqu’au mois d’avril 2021. Le suivi intégral de ce dispositif vous fera bénéficier d’une attestation de réussite.</p>
                    </div>
                  </div>
                </div>
                                
                 
               </section>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<?php include "footer.php"; ?>